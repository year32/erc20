

// SPDX-License-Identifier: MIT
pragma solidity 0.5.5;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title Tax Token Contract
 */
contract TaxToken is ERC20 {
    string public name = "Tax Token";
    string public description = "A Token For Tax Remittance";
    string public symbol = "TX";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;
    uint256 public taxPercentage = 5; // 5% tax

 constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }


    function transferWithTax(address recipient, uint256 amount) public returns (bool) {
        uint256 taxAmount = amount * taxPercentage / 100;
        uint256 totalAmount = amount + taxAmount;

        _transfer(msg.sender, recipient, totalAmount);
        return true;
    }
}

