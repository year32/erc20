

const TaxToken = artifacts.require("TaxToken");

contract("TaxToken", async (accounts) => {
  let taxToken;
  const initialSupply = 10000000;
  const taxPercentage = 5;

  beforeEach(async () => {
    taxToken = await TaxToken.new();
  });

  it("should have correct initial supply", async () => {
    const totalSupply = await taxToken.totalSupply();
    assert.equal(totalSupply.toNumber(), initialSupply);
  });

  it("should transfer tokens with tax", async () => {
    const recipient = "0x45f06EA037b55ac3f447BCe449E803A26ACC2F59";
    const amount = 1000 * 10 ** 2; // 1000 tokens

    await taxToken.transferWithTax(recipient, amount);

    const expectedBalance = amount + (amount * taxPercentage / 100);
    const balance = await taxToken.balanceOf(recipient);
    assert.equal(balance.toNumber(), expectedBalance);
  });
});




// const TaxToken = artifacts.require("TaxToken");

// contract("TaxToken", async (accounts) => {
//   const sender = accounts[0];
//   const recipient = accounts[1];

//   // Test that the constructor initializes the contract correctly.
//   it("should initialize the contract correctly", async () => {
//     const taxToken = await TaxToken.new();

//     assert.equal(taxToken.name(), "Tax Token");
//     assert.equal(taxToken.description(), "A Token For Tax Remittance");
//     assert.equal(taxToken.symbol(), "TX");
//     assert.equal(taxToken.decimals(), 2);
//     assert.equal(taxToken.totalSupply(), 10000000);
//   });

//   // Test that the transferWithTax() function transfers the correct amount of tokens, including the tax.
//   it("should transfer the correct amount of tokens, including the tax", async () => {
//     const taxToken = await TaxToken.new();

//     // Mint some tokens for the sender.
//     await taxToken.mint(sender, 10000);

//     // Transfer 500 tokens to another address.
//     await taxToken.transferWithTax(recipient, 500);

//     // Check that the sender's balance is correct.
//     const senderBalance = await taxToken.balanceOf(sender);
//     assert.equal(senderBalance, 9450);

//     // Check that the recipient's balance is correct.
//     const recipientBalance = await taxToken.balanceOf(recipient);
//     assert.equal(recipientBalance, 550);
//   });

//   // Test that the transferWithTax() function fails if the sender does not have enough tokens to cover the transfer and the tax.
//   it("should fail if the sender does not have enough tokens", async () => {
//     const taxToken = await TaxToken.new();

//     // Mint some tokens for the sender.
//     await taxToken.mint(sender, 1000);

//     // Try to transfer 1500 tokens to another address.
//     try {
//       await taxToken.transferWithTax(recipient, 1500);
//     } catch (error) {
//       assert.equal(error.message, "ERC20: transfer amount exceeds balance");
//     }
//   });
// });
